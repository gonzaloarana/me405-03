''' @file main.py
    
    Lab 1 main file used for testing of motor class.

    This file runs tests on creating and controlling a motor class.'''

import time
from motor import Motor
import pyb

def main():
    ''' This method uses the motor class to create and test a motor object.
        The test consists of running the motor forward at 30% duty cycle
        for 3 seconds, stopping the motor 1 second, and then running the motor
        backward at 30% duty cycle for 3 seconds. Additionally an invalid motor duty
        cycle is used to show the error handling.'''
    # Create the pin objects used for interfacing with the motor driver
    pin_EN  = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)

    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq = 20000)

    # Create a motor object passing in the pins and timer
    moe = Motor(pin_EN, pin_IN1, pin_IN2, tim)
    # Enable motor
    moe.enable()
    # Set the duty cycle to 30 percent forward
    print('Setting 30% forward')
    moe.set_duty(30)
    #wait 3 seconds 
    time.sleep(3)
    print('stopping motor')
    moe.set_duty(0)
    time.sleep(1)
    # Set the duty cycle to 30 percent backward
    print('Setting 30% backward')
    moe.set_duty(-30)
    #wait 3 seconds 
    time.sleep(3)
    # Attempt to set the duty invalid
    print('Setting 400% forward')
    moe.set_duty(400)
    #wait 2 seconds 
    time.sleep(2)
    #Disable motor
    moe.disable()

if __name__ == '__main__':
    main()
