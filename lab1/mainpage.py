''' @file mainpage.py
 Lab1 - Motor Driver

 @mainpage

 @section sec_intro Introduction
 This lab focuses on creating a motor interface class to easily
 enable, disable, and change the duty cycle of a given motor.

 Link to code: https://bitbucket.org/gonzaloarana/me405-03/src/master/lab1/
 
 @author Gonzalo Arana

 @date April 22, 2020

'''
