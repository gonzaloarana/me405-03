''' @file motor.py
  This file contains the motor class interface that enables, disables, and
  sets motor duty cycles (in both positive and negative direction).'''
import pyb

class Motor:
    ''' This class implements a motor driver for the ME405 board. '''
    #The pins below store pyb Pin objects to control the motor
    EN_pin = None
    IN1_pin = None
    IN2_pin = None
    
    #timer stores the given reference to a timer
    timer = None
    
    #tch1 and tch2 store references to timer channel objects
    tch1 = None
    tch2 = None

    def __init__(self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        
        @param EN_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin 
                     and IN2_pin. '''
        print ('Creating a motor driver')
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        self.tch1 = timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
        self.tch2 = timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
        self.EN_pin.low()
        
    def enable(self):
        ''' This method enables the motor.'''
        print ('Enabling Motor')
        self.EN_pin.high()

    def disable(self):
        ''' This method disables the motor.'''
        print ('Disabling Motor')
        self.EN_pin.low()

    def set_duty(self, duty):
        ''' This method sets the duty cycle to be sent to the motor to the 
        given level. Positive values cause effort in one direction, 
        negative values in the opposite direction.
        @param duty A signed integer holding the duty 
           cycle of the PWM signal sent to the motor'''
        if(duty < -100 or duty > 100):
            print('Invalid duty cycle. Must be between -100 and 100.')
        elif(duty > 0):
            #going forward
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(duty)
        elif(duty < 0):
            #going backward
            #take absolute value of duty
            duty = duty * -1
            self.tch1.pulse_width_percent(duty)
            self.tch2.pulse_width_percent(0)
        else:
            #idling the motor
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(0)
