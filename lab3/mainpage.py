''' @file mainpage.py
    @author Gonzalo Arana
    @mainpage
    @section sec_intro Introduction
    Lab 3 - Closed Loop Controller
    This lab focuses on creating an Closed Loop Controller that is used to change
    the response of the motor actuation as it approaches a target motor position.

    A Demo of this can be found at: https://youtu.be/oDeipwizefE

    @section Purpose
    This controller is intended to smoothly and quickly actuate a motor to the
    requested position
    
    @section Usage
    The Closed Loop Controller object can be found/used by importing Closed Loop Controller from closedloopcontroller.py.
    A Closed Loop Controller object takes and stores a kp gain value. This gain value is then used
    to calculate actuation when the update() function is called with a setpoint and the current position
    of the motor.
    
    @section Testing
    This code was tested by one motor and encoder and running a while loop to test various
    kp values. Included in the repository are the images 11.png and 13.png that show the results
    of step response testing with a kp of 0.13 and 0.11.
    
    @section Bugs and Limitations
    Because the main function runs in a while loop that is looking for the target and actual position to be within 10 rotations of one another,
    if the gain value causes an undershoot or overshoot of the target position, the test will never finish. 

    @section Location
    All code can be found at the link below.
    Link to code: https://bitbucket.org/gonzaloarana/me405-03/src/master/lab3/
'''
