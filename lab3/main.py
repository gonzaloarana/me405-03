''' @file main.py
    
    Lab 3 main file used for testing of ClosedLoopController class.

    This file runs tests on creating and using a ClosedLoopController'''

import time
from encoder import Encoder
from motor import Motor
from closedloopcontroller import ClosedLoopController
import pyb

def main():
    ''' This method uses the motor, encoder, and closed loop controller classes to 
        test and print step responses of kp values'''
    running = True

    while(running):
        response = 'a'
        print("Input kp to test or q to quit: ")
        response = input()

        if (response == 'q' or response == 'Q'):
            running = False
        else:
            print(response)
            test_kp(float(response))
        #else:
        #    print('Invalid input.')
        
    

def test_kp(kp):
    ''' This method sets up a motor driver, encoder driver, and closed loop controller. It
    then runs a step response test using the given kp by updating that actuator value every
    10ms until the requested motor position is reached. Finally, the method prints a record
    of the motor position every 10ms that the test occured.
    @param kp The gain value to be tested'''
    
    steady = False
    timer = 0
    setpoint = 3400

    # tuple list of time, position values
    records = []

    # setup motor
    # -------------------
    # Create the pin objects used for interfacing with the motor driver
    pin_EN  = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)

    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq = 20000)

    # Create a motor object passing in the pins and timer
    moe = Motor(pin_EN, pin_IN1, pin_IN2, tim)
    # Enable motor
    moe.enable()

    # setup encoder
    # -------------------
    # Note: Timer 8 can be used with PC6 and PC7 as channels 1 and 2, respectively
    pin_PC6 = pyb.Pin(pyb.Pin.cpu.C6, pyb.Pin.AF_PP, pull = pyb.Pin.PULL_NONE, af=pyb.Pin.AF3_TIM8) #T8 Ch1
    pin_PC7 = pyb.Pin(pyb.Pin.cpu.C7, pyb.Pin.AF_PP, pull = pyb.Pin.PULL_NONE, af=pyb.Pin.AF3_TIM8) #T8 Ch2

    t8 = pyb.Timer(8, prescaler = 1, period = 0xffff)
    
    e1 = Encoder(pin_PC6, pin_PC7, t8)

    # update encoder
    e1.update()
    e1.set_position(0)
    
    # setup closed loop controller
    loopcontrol = ClosedLoopController(kp)

    while(not steady):
        e1.update()
        pos = e1.get_position()
        records.append((str(timer), str(pos)))
        
        if (pos > setpoint - 10 and pos < setpoint + 10):
            print('done')
            steady = True
        else:
            timer += 10
            actuate = loopcontrol.update(setpoint, pos)
            moe.set_duty(actuate)
            #utime.sleep_ms(0.010)
            time.sleep(0.010)

    #print results
    print('Results for kp: ' + str(kp) + '---------------')
    print('time, position')
    for timer, pos in records:
        print(timer + ', ' + pos)

    # Disable motor
    moe.disable()

if __name__ == '__main__':
    main()
