''' @file encoder.py
  This file contains the motor class interface that enables, disables, and
  sets motor duty cycles (in both positive and negative direction).'''

import pyb

class Encoder:
    ''' This class implements a encoder driver for the ME405 board. '''
    #class counter that keep track of position without overflowing
    position = 0
    
    #stores the last two previous count values seen by the encoder
    history = [0, 0]
    
    #keeps idx of the next value to be written to
    next_idx = 0
    
    #The pins below store pyb Pin objects that interact with the encoder
    pch1 = None
    pch2 = None
    
    #timer stores the given reference to a timer
    timer = None
    
    #tch1 store a reference to timer channel objects
    tch1 = None

    def __init__(self, pch1, pch2, timer):
        ''' Creates an encoder driver and saves the pins for channel 1
        and 2 as well as the associated timer. Finally, this function
        configures the timer to react to changes in encoder channels
        A and B.
        
        @param pch1 A pyb.Pin object to interact with chA of the encoder 
        @param pch2 A pyb.Pin object to interact with chB of the encoder
        @param timer A pyb.Timer object to keep count'''
        print ('Creating an encoder driver')
        self.pch1 = pch1
        self.pch2 = pch2
        self.timer = timer
        self.tch1 = timer.channel(1, pyb.Timer.ENC_AB)
    
    def update(self):
        ''' Updates the recorded position of the encoder. '''
        #Grab and save the latest encoder position
        self.history[self.next_idx] = self.timer.counter()
        
        #Update index
        self.next_idx = (self.next_idx + 1) % 2
        
        #figure out the delta
        delta = self.get_delta()
        
        #print(delta)
        
        #update the objects true positon
        self.position += delta
    
    def get_position(self):
        ''' Returns the most recently updated position of the encoder.'''
        return self.position
    
    def set_position(self, position):
        ''' Resets the position to to a speci?ed value.
        @param position The value that position should be set to.'''
        self.position = position
        
    def get_delta(self):
        ''' Returns the difference in recorded position between the
        two most recent calls to update().'''
        #Maximum possible value of our counter. Used in under/overflow calculation
        max_count = 65535
        
        '''The last and second to last recorded positions
        consider the following delta ( 10 -> 120)
        count 1 (the last postion seen) is 120
        count 2 (the second to last position seen) is 10'''
        count_1 = self.history[self.next_idx - 1]
        count_2 = self.history[self.next_idx - 2]
        
        #Used to update global position
        delta = 0
        
        #print(str(self.history))
        
        if(count_2 < count_1):
            '''Case one has two possibilities:
             s1) positive delta ( 10 -> 120)
             2) underflow ( 5 -> 65500)'''
            delta = count_1 - count_2
        
            #underflow definitely occured
            if delta > 25000:
                delta = count_2 - count_1 + max_count
        elif(count_1 < count_2):
            '''Case two also has two possiblities
             1) negative delta ( 120 -> 10)
             2) overflow ( 65500 -> 5)'''
            delta = count_1 - count_2
            
            #overflow definitely occured:
            if delta < -25000:
                delta = count_1 + max_count - count_2
                
        return delta