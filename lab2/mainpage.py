''' @file mainpage.py
    @author Gonzalo Arana
    @mainpage
    @section sec_intro Introduction
    Lab 2 - Encoder Driver
    This lab focuses on creating an encoder driver that can be easily use to track absolute position of
    our motors as they spin.

    @section Purpose
    This driver is intended to create an encoder object that can easily track the absolute positon of a motor
    as it spins.
    
    @section Usage
    The Encoder object can be found/used by importing Encoder from encoder.py.
    In order to create an encoder object a valid combination of pins and timer to use must be given as arguments.
    After that point, the update function can be called constantly to update the true position of the
    motor as it moves.
    
    @section Testing
    This code was tested by connecting two motors and encoders, constantly calling the update function for both,
    and printing the results. Meanwhile, the encoders were moved by hand to ensure that the overflow, underflow,
    increasing, and decreasing position calculations all behaved as expected.
    
    @section Bugs and Limitations
    The get_delta() function assumes that any deltas of greater than 25000 or less than -25000 are cases
    of the latter are underflow and overflow, respectively. This means that motors running at frequencies higher
    than 25kHz will likely start experience buggy position encoding. This has not been completely verified yet.
    
    @section Location
    All code can be found at the link below.
    Link to code: https://bitbucket.org/gonzaloarana/me405-03/src/master/lab2/
'''
