''' @file main.py
    
    Lab 2 main file used for testing of encoder class.

    This file runs tests on creating and controlling an encoder class.'''

import time
from encoder import Encoder
import pyb

def main():
    ''' This method uses the encoder class to create and test two encoder objects.
        The test consists of creating an encoder for two motors. The first is on pins PB6/7
        and uses timer 4. The second is on pins PC6/7 and uses timer 8'''
    
    #Note: Timer 4 can be used with PB6 and PB7 as channels 1 and 2, respectively
    pin_PB6 = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.AF_PP, pull = pyb.Pin.PULL_NONE, af=pyb.Pin.AF2_TIM4) #T4 Ch1
    pin_PB7 = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.AF_PP, pull = pyb.Pin.PULL_NONE, af=pyb.Pin.AF2_TIM4) #T4 Ch2

    t4 = pyb.Timer(4, prescaler = 1, period = 0xffff)
    
    e1 = Encoder(pin_PB6, pin_PB7, t4)
    
    
    #Note: Timer 8 can be used with PC6 and PC7 as channels 1 and 2, respectively
    pin_PC6 = pyb.Pin(pyb.Pin.cpu.C6, pyb.Pin.AF_PP, pull = pyb.Pin.PULL_NONE, af=pyb.Pin.AF3_TIM8) #T8 Ch1
    pin_PC7 = pyb.Pin(pyb.Pin.cpu.C7, pyb.Pin.AF_PP, pull = pyb.Pin.PULL_NONE, af=pyb.Pin.AF3_TIM8) #T8 Ch2

    t8 = pyb.Timer(8, prescaler = 1, period = 0xffff)
    
    e2 = Encoder(pin_PC6, pin_PC7, t8)
    
    counter = 0
    
    #contantly test and output encoder positions
    while(counter < 30):
        counter += 1
        e1.update()
        e2.update()
        print("Encoder 1 value: " + str(e1.get_position()))
        print("Encoder 2 value: " + str(e2.get_position()))
        print("--------------------------------")
        #wait 2 seconds 
        time.sleep(2)
        

if __name__ == '__main__':
    main()
