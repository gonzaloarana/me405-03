''' @file main.py
 This file contains the required code for lab 0. This script 
 contiously asks the user which Fibonacci number they would like calculated,
 prompts them if they have provided invalid input, and quits when requested.

    @author Gonzalo Arana

    @date April 22, 2020'''

def fib (idx):
    '''This function calculates a Fibonacci number corresponding to
    a specified index.

    @param idx An integer specifying the index of the desired
               Fibonacci number.'''

    print('\nCalculating Fibonacci number at '
          'index n = {:}.'.format(idx))
    
    fibValue = fib_helper(idx)

    print('The Fibonacci number at index'
          ' n = {:} is: {:}\n'.format(idx, fibValue))

def fib_helper(idx):
    '''This function is called recursively to calculate a Fibonacci number 
    of the given index.

    @param idx An integer specifying the index of the desired
           Fibonacci number.'''

    if idx == 0:
        return 0
    elif idx == 1 or idx == 2:
        return 1
    else:
        return fib_helper(idx - 1) + fib_helper(idx - 2)

if __name__ == '__main__':
    #Continously get input from user until they quit
    running = True

    while(running):
        userInput = input("Enter an index for the desired Fibonnaci number or Q/q to quit: ")
        
        if(userInput.lower() == 'q'):
            running = False
        elif(userInput.isdigit() and int(userInput) >= 0):
            fib(int(userInput))
        else:
            print("\nThe value {:} is invalid."
                  " Acceptable inputs: an integer greater than 0 or Q/q.".format(userInput))

    print("\nClosing Fib program. Goodbye!")

    exit(0)
