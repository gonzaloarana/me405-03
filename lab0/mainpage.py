''' @file mainpage.py
 Lab0 - Fibonacci Generator

 detailed doc

 @mainpage

 @section sec_intro Introduction
 This is the first lab in the ME405 Mechatronics class. This program constantly asks a user for the index
 of a fibonacci number they would like to generate. If the index is invalid, the user is prompted to input
 a valid index. If the user wishes to terminate the program, they can type in q or Q.

 @author Gonzalo Arana

 @date April 22, 2020

'''
