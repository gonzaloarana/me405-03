''' @file update_motor.py
@brief    Task for checking if motor needs to be turned off/on
@details  Motor rotation should be udpated based on the value of the 
	  spintray boolean in the motor class. If the motor needs to be 
	  turned, a constant, slow speed should be set by the closed loop 
	controller. '''

import utime

class Update_Motor:
    ''' This class contains a task responsible for checking if the spintray 
		motor needs to be turned on.'''
    
    def __init__(self, ultrasonic, motor, encoder, loopController):
        ''' Initializes this task and a counter for how many time it has 
	    been called. Also stored a copy of the ultrasonic, motor, 
	    encoder, and closed loop controller objects to read from.

	@param ultrasonic An ultrasonic sensor object
	@param motor A motor object
	@param encoder An encoder object
	@param loopController A loop controller object'''   
        self.runs = 0
        self.setpoint = 100
        self.actuate = 0
        self.ultrasonic = ultrasonic
        self.motor = motor
        self.encoder = encoder
        self.loopController = loopController

    def run(self):
	''' This function handles motor enabling, disabling, and tuning
	    of actuation values'''
        if self.runs % 140 == 0:
            #get state variable of system
            debounce = self.ultrasonic.debouncing
            dist = self.ultrasonic.distance
            spin = self.motor.spintray

            #Handle cases if spintray was just toggled on or off
            if(not debounce and spin and dist < 20):
                #turn on motor
                self.motor.enable()
                print('Motor on')
            elif(not debounce and not spin and dist <20):
                #shutoff motor and reset encoder
                self.actuate = 0    
                self.motor.disable()
                self.encoder.set_position(0)
                print('Motor off')
            
            #if motor is running
            if(spin):
                print('updating spin')
                #update encoder
                self.encoder.update()
            
                #if encoder is getting close to setpoint, reset it
                if(self.encoder.get_position() > 60):
                    self.encoder.set_position(0)

                position = self.encoder.get_position()
                print('current pos: ' + str(position))

                #get next actuation value
                actuate = self.loopController.update(self.setpoint, position)
                actuate = actuate * 1
                #update motor acutation value
                self.motor.set_duty(actuate)

            
