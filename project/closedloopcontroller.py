''' @file closedloopcontroller.py
  This file contains the closed loop controller class which provides actuation values
  based on a given controller gain and position values'''

class ClosedLoopController:
    ''' This class implements a closed loop controller for the ME405 board. '''
    # Stores a KP value for the controller
    kp = None

    def __init__(self, kp):
        ''' Creates a closed loop controller 
        
        @param kp Value for the controller gain.'''

        print ('Creating a closed loop controller')
        self.kp = kp
        
    def update(self, setpoint, actualPosition):
        ''' This method taskes a target encoder position (setpoint) and the current
        position of the encoder (actualPosition) and calculates an actuation value using
        gain and error values to direct the motor the target position.
        @param setpoint A signed integer for the target encoder position
        @param actualPosition A signed integer for the current encoder position'''

        #print('target' + str(setpoint))
        #print('current' + str(actualPosition))
    
        error = actualPosition - setpoint 
        #print(error)
        
        if not (actualPosition >= 0 and setpoint >= 0):
            error = error * -1
        #print('kp: ' + str(self.kp))
        actuation = int(error * self.kp)
        
        if actuation > 100:
            actuation = 100
        if actuation < -100:
            actuation = -100
        
        if (setpoint == actualPosition):
            return -101
        
        if (actuation == 0 and setpoint is not actualPosition):
            actuation = 3 * (actualPosition - setpoint)
        #print('act: ' + str(actuation))
        return actuation
