''' @file update_spintray.py
 Task for checking if the bot spintray needs to be turned off/on
 If the sonar sensor detects an object within 20cm and the system 
 is not debouncing, in otherwords, if the this is the first time
 the system sees the object, then the spintray should be toggled 
 on or off. '''

import utime

class Update_Spintray:
    ''' This class contains a task responsible for checking if the spintray operation should be toggled.'''
    
    def __init__(self, ultrasonic, motor, encoder):
        ''' Initializes this task and a counter for how many time it has been called. Also stored a copy of
            the ultrasonic sensor, motor, and encoder objects to read from.   

	@param ultrasonic An ultrasonic sensor object
	@param motor A motor object
	@param encoder An encoder object'''
        self.runs = 0
        self.ultrasonic = ultrasonic
        self.motor = motor
        self.encoder = encoder

    def run(self):
	''' This function enables or disables the spintray while ignoring any
	    duplicate requests to do either. '''
        if self.runs % 90 == 0:
            debounce = self.ultrasonic.debouncing
            dist = self.ultrasonic.distance
            
            #If this is the first occurance of an object within 20cm
            if(not debounce and dist < 20):
                #toggle spintray boolean
                self.motor.spintray = not self.motor.spintray
                
                #If turning off spintray and reset encoder position.
                #This ensures that the closed-loop controller is less likely
                #to reach its destination.
                if (self.motor.spintray == False):
                    print('Spintray turned off')
                    self.encoder.set_position(0)
                else:
                    print('Spintray turned on')
