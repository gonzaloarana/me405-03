''' @file update_debouncing.py
@brief    Task for checking if deboucing needs to be turned off/on
@details  If the sonar sensor was just activated within 20cm, we 
	  need to turn on deboucing to ensure we aren't toggling 
	  states endlessly. While this is occuring, the debouncing 
	  boolean needs to be set to true  
            
'''

import utime

class Update_Debouncing:
    ''' This class contains a task responsible for checking if software 
	debouncing should be turned on or off'''
    
    def __init__(self, ultrasonic):
        ''' Initializes this task and a counter for how many time it has 
	    been called. Also stored a copy of the ultrasonic sensor 
	    object to read from.
			
	@param ultrasonic An ultrasonic sensor object'''
        self.runs = 0
        self.ultrasonic = ultrasonic

    def run(self):
	''' This function prevents ultrasound readings from overloading
	    the state machine of the system by enabling/disabling
	    debouncing.'''
        if self.runs % 100 == 0:
            if(self.ultrasonic.debouncing == False):
                if(self.ultrasonic.distance < 20):
                    self.ultrasonic.debouncing = True
                    print('Debouncing on')
            else:
                if(self.ultrasonic.distance > 20):
                    self.ultrasonic.debouncing = False
                    print('Debouncing off')

