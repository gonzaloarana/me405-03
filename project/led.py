import pyb

class LED:
    ''' This class is an interface for connecting to and updating a ring/strip of LED's '''

    pin = None
    count = 0

    def __init__ (self, pin, count):
        ''' Creates an LED driver that saves the GPIO pin and count of LED's in the strip/circle
        @param pin A pin object that sends data to the LEDs
        @param count The total number of LEDs present in the strips/circles connected to one pin'''
        self.pin = pin
        self.count = count  

    def set_all(self, color):
        ''' Turns all of the leds in the unit on to the specified color with (0, 0, 0) being off.
        @param color The requested color for all lights in a touple containing either RBG 
               values or RGBW or RGBY values depending on the specs of the LED.'''
        for ledNUM in range(self.count):
            print('\nSending LED #' +  str(ledNUM) + '\n------------')            
            self.send_color_tuple(color)

    def send_color_tuple(self, color_tuple):
        ''' Breaks each color into RGB values, and sends one bit at a time. One full transmission
            is 24 bits, 8 for green, 8 for red, and 8 for blue, sent in that order.
            @param color_tuple A tuple containing the requested LED color (R, G, B)'''
        #send green
        print('sending green')
        self.send_color_byte(color_tuple[1])    
        #send red
        print('sending red')
        self.send_color_byte(color_tuple[0])
        #send blue
        print('sending blue')
        self.send_color_byte(color_tuple[2])

            
    def send_color_byte(self, color_byte):
        ''' Sends one byte worth of data (8 bits) that represent either R, G, or B. 
        @param color_byte the 8 bits that should be sent'''
        bitMask = 200

        for bitNum in range(8):
            data = color_byte & bitMask
            if (data == 0):
                self.send_low()
                print('sent 0')
            else:
                self.send_high()
                print('sent 1')
            color_byte = color_byte << 1    

    def send_high(self):
        ''' Uses the WS2812 LED hold times required to send a high signal (1)'''        
        self.pin.high()
        #high time delay
        pyb.udelay(0.85)
        self.pin.low()
        #low time delay
        pyb.udelay(0.75)    

    def send_low(self): 
        ''' Uses the WS2812 LED hold times required to send a low signal (0)'''
        self.pin.high()
        #high time delay
        pyb.udelay(0.45)
        self.pin.low()
        #low time delay
        pyb.udelay(0.95)
