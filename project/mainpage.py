''' @file mainpage.py
    @author Gonzalo Arana
    @mainpage
    @section Introduction
    Final Project: Showroom Pedestal.
    
    This page describes the breakdown for my final project.

	@section Demo
	Demo videos of the hardware can be found at the links below. Two have been taken,
	one before adding the hardware into the wooden enclosure and one afterwards. 
	
	Demo without enclosure: https://youtu.be/Ge1Epx4D80w

	Demo with enclosure: https://youtu.be/kCdsCzFvYS8

    @section Problem Statement
    My desk space is a little bland and doesn't really have a personal touch to it yet.
    To give it a bit more flare I am making a 3.5 square inch cube-shaped pedestal that
    has a circular tray on the top to show off any personal work or toy car models,
    showroom style. And to make it even more interesting, the pedestal will be able to
    toggle between a couple of directional and speed varying modes without the use of
    a button.
    
    @section Requirements 
    In order to get the showroom spinning tray effect, I will be using a motor to rotate
    a circular plate. This motor will need to run on a closed loop controller to ensure
    that there is a smooth transition when toggling between spinning modes.  These two
    fulfill the requirement of using actuators and a closed loop controller. In order to
    achieve the button-free toggle, I will use an ultrasound sensor that just checks if
    there is an object within 4 inches in front of the cube. When system notices a change
    in the presence of an object within 4 inches, it will treat that as a toggle signal.
    This fulfills the requirement of implementing a sensor not used in this class.
    
    @section Materials 
    In order to start this project I will need to get a hold of a sturdy square base and
    circular tray, an ultrasonic sensor, some plastic or metal to mount the motor and plate,
    and potentially some batteries to run the motor and microcontroller. For now I will
    likely just use the wall adapter and a power bank to power the motor and microcontroller.
    I currently have most if not all of the part I need including a 5V battery bank,
    ultrasound sensor, a plastic wheel that I will use as a tray mount, spare wood, and
    a table saw. The only thing I might need to purchase or craft are a motor mount to
    hold the motor in a vertical position (see diagram above) and some screws to hold the
    box together. I would put together a Bill of Materials for this, but I am unsure what
    size motor mount and screws to buy at the moment. 
    
    @section Manufacturing
    To have a sturdy base, I plan on using some spare pieces of wood, table saw, screwdriver,
    screws, and some metal or plastic to hold the motor and rotating tray in place. I may also
    need a small battery bank to power the circuit.  

    @section Timeline
    Date ............... Milestone
    
    27 May 2020 ---- Order Electronic Parts
    
    30 May 2020 ---- Complete Wooden Housing
    
    3 June 2020 ---- Complete Code Prototype
    
    4 June 2020 ---- Begin Testing 
    
    9 June 2020 ---- Finish Testing, Begin Final Assembly
    
    10 June 2020 ---- Finalize Assembly
    
    12 June 2020 ---- Turn In Project Report

	@section Bugs or Issues
	In order to get the LED light strips working I attempted to use a micropython library 
	called neopixels. This unfortunately was unavailable in micropython, so I ended up writing
	a low level driver file 'led.py' based on the datasheet for the led's I purchased. I was
	unable to get the timing set up properly, so the LED's are not working currently.

'''
