''' @file ultrasonic.py
 This file contains an ultrasonic sensor driver.'''

import pyb
import time
import machine

class Ultrasonic:
    ''' This file contains the HC-SR04 Ultrasonic Sensor driver.
        This sensor has a range of 2cm-4m'''

    triggerPin = None
    echoPin = None
    timeout_us = 0
    high = 1
    low = 0
    distance = 0
    debouncing = False

    def __init__(self, triggerPin, echoPin, timeout_us = 60000):
        ''' Creates a new Ultrasonic driver object.

        @param triggerPin The output pin that sends a 10uS pulse
        @param echoPin The input pin that listens for the 10uS pulse to come back
        @param timeout_us The maximum amount of time to wait for the pulse. 
               Datasheet recommends 60mS measurement intervals.'''
        
        self.triggerPin = triggerPin
        self.echoPin = echoPin
        self.timeout_us = timeout_us

    def transmitAndReceive(self):
        ''' Send a 10uS pulse from the trigger pini and measures its length
            when if it returns before the timeout'''
        self.triggerPin.low()
        time.sleep_us(1)
        
        self.triggerPin.high()
        time.sleep_us(10)
        self.triggerPin.low()
        
        echo_us = machine.time_pulse_us(self.echoPin, self.high, self.timeout_us)
        print(echo_us)
        
        #save a copy of distance for round robin task usage
        if(echo_us == -1):
            print('A signal came back. Timed out while reading it.')
            self.distance = 0
            return 0
        elif(echo_us == -2):
            print('Timed out before any signal came back.')
            self.distance = 0
            return 0
        else:
            self.distance = self.echoToCM(echo_us)
            return self.distance
        
            
    def echoToCM(self, echo_us):
        ''' Converts the given echo pulse measurement from microseconds to a 
            distance measurement in cm.

        @param echo_us The echo pulse with in microseconds'''
        
        return (echo_us/ 2) / 29.1
    
    def setDebouncing(self, value = True):
        ''' Set the debouncing flag to whatever value is passed through, True by default.

        @param value The boolean value to update debouncing flag'''
        
        self.debouncing = value
    
    def getDebouncing(self):
        ''' Returns the current value of debouncing'''
        
        return self.debouncing
        
