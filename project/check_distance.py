''' @file check_distance.py
@brief    Task for checking the sonar sensor
@detailsUpdates the distance currently seen by the sonar sensor
            
'''

import utime

class Check_Distance:
    ''' This class contains a task responsible for checking if the 
	ultrasonic sensor is being blocked (toggled). Blocking this
	sensor at a distance of less than 20cm signifies that the 
	user want's to turn on the bot.'''
    
    def __init__(self, ultrasonic):
        ''' Initializes this task and a counter for how many time 
	    it has been called. Also stored a copy of the ultrasonic 
	    sensor object to read from.

	@param ultrasonic An ultrasonic sensor object'''   
        self.runs = 0
        self.ultrasonic = ultrasonic
        self.count = 0

    def run(self):
	''' This function periodically queries the ultrasonic sensor
	    to see if there is an object in its path'''
        if self.runs % 130 == 0:
            self.count += 1
            self.ultrasonic.transmitAndReceive()
            if self.ultrasonic.distance == 0:
                print('No object detected')
            else:
                print('Object at: ' + str(self.ultrasonic.distance) + 'cms')
    
