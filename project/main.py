''' @file main.py
    
    Final Project main that handles controls for the showcase bot.

    This file is used to test each individial component and run
    the handle the state changes of the showcase bot.'''

from encoder import Encoder
from motor import Motor
from closedloopcontroller import ClosedLoopController
from led import LED
from ultrasonic import Ultrasonic
import pyb
import time, utime

from update_debouncing import Update_Debouncing
from check_distance import Check_Distance 
from update_spintray import Update_Spintray
from update_motor import Update_Motor

def main():
    ''' This method controlls the states of the showcase bot'''
    running = True
    count = 0
    setpoint = 100
    kp = 0.2	

    #variables used for LED
    #blue = (0, 0, 255)
    #off = (0, 0, 0)

    #setup tasklist
    task_list = [Update_Debouncing(ultra), Check_Distance(ultra), 
		 Update_Spintray(ultra, motor, encoder), Update_Motor(ultra, motor, encoder, loopcontrol)]

    #setup all hardware
    #led = LEDSetup()
    motor = motorSetup()
    encoder = encoderSetup()
    loopcontrol = ClosedLoopController(kp)
    ultra = ultraSonicSetup()

    '''NOTE: The code below was for testing LED using using the protocol
    described in the datasheet. I was unable to get this to work, so its 
    commented out. Difficult to debug without scope.'''
    #ledRing = LEDSetup()
    #ledRing.set_all(blue)
    #time.sleep(5)      
    #ledRing.set_all(off)

    #setup timing variables
    ''' @brief   Task loop rate in uS
     @details A Python integer that represents the nominal number of microseconds
	    between iterations of the task loop.'''
	
    interval = 10000

    ''' @brief   The timestamp of the next iteration of the task loop
     @details A value from utime.ticks_us() specifying the timestamp for the next
	      iteration of the task loop.
	 
     This value is updated every time the task loop runs to schedule the
    next iteration of the task loop.'''

    next_time = utime.ticks_add(utime.ticks_us(), interval)

    '''  @brief   The timestamp associated with the current iteration of the task
	        loop
     @details A value from utime.ticks_us() updated every time the task loop 
	      checks to see if it needs to run. As soon as this timestamp exceeds
	     next_time the task loop runs an iteration.'''
	
    cur_time = utime.ticks_us()

    #run round robin scheduling loop
    while True:
        # Update the current timestamp
	cur_time = utime.ticks_us()
		
	# Check if its time to run the tasks yet
	if utime.ticks_diff(cur_time, next_time) > 0:
			
            # If it is time to run, update the timestamp for the next iteration and
	    # then run each task in the task list. 
	    next_time = utime.ticks_add(next_time, interval)
		    	
	    for task in task_list:
                task.run()

    '''NOTE: This code below is for debugging. This oldmain() used 
    to test samples of the ultrasonic sensor
    #setup all the tasks in our scheduler
    debounce_task = Update_Debouncing(ultra)
    distance_task = Check_Distance(ultra)
    spintray_task = Update_Spintray(ultra, motor, encoder)
    motor_task = Update_Motor(ultra, motor, encoder, loopcontrol)

    #Run for a 10 distance measurements
    while(running):
        distance_task.run()
        spintray_task.run()
        motor_task.run()
        debounce_task.run()
        time.sleep(1)
        if (distance_task.count > 10):
            running = False 
	'''

def ultraSonicSetup():
    ''' This method sets up the ultrasonic sensor and its pins'''
    
    TriggerPin = pyb.Pin(pyb.Pin.cpu.A8, pyb.Pin.OUT_PP)
    EchoPin = pyb.Pin(pyb.Pin.cpu.B10, pyb.Pin.IN)
    return Ultrasonic(TriggerPin, EchoPin)
        
def LEDSetup():
    ''' This method sets up the led and its pins'''
    
    pin_LED = pyb.Pin(pyb.Pin.cpu.B8)   
    LEDnum = 12 
    return LED(pin_LED, LEDnum)

        
def motorSetup():
    ''' This method sets up the motor and its pins'''
	
    # setup motor
    # -------------------
    # Create the pin objects used for interfacing with the motor driver
    pin_EN  = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)

    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq = 20000)

    # Create a motor object passing in the pins and timer
    moe = Motor(pin_EN, pin_IN1, pin_IN2, tim)
    # Enable motor
    moe.enable()

    return moe

def encoderSetup():
    ''' This method sets up the encoder and its pins'''
    
    #Note: Timer 4 can be used with PB6 and PB7 as channels 1 and 2, respectively
    pin_PB6 = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.AF_PP, pull = pyb.Pin.PULL_NONE, af=pyb.Pin.AF2_TIM4) #T4 Ch1
    pin_PB7 = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.AF_PP, pull = pyb.Pin.PULL_NONE, af=pyb.Pin.AF2_TIM4) #T4 Ch2

    t4 = pyb.Timer(4, prescaler = 1, period = 0xffff)
    
    e1 = Encoder(pin_PB6, pin_PB7, t4)
 
    # update encoder
    e1.update()
    e1.set_position(0)

    return e1    
    
if __name__ == '__main__':
    main()
