''' @file main.py
    
    Lab 4 main file used for testing of IMU class.

    This file runs tests on creating and using an IMU object'''

import time
import pyb
from pyb import I2C
from imu import IMU

#TODO: connect BNO055 to:
#       - GND: Pin
#       - VCC (3.5?): Pin 
#       - SDA: Pin 
#       - SCL: Pin

def main():
    ''' This method uses the motor, encoder, and closed loop controller classes to 
        test and print step responses of kp values'''
    running = True
    timer = 0

    print("Starting IMU Test")

    i2c = I2C(1)

    #Start I2C w/ microcontroller as master and baudrate 
    #TODO: Check data sheet for baudrate of BNO Sensor
    i2c.init(I2C.MASTER)

    #Check addresses of connected devices
    #devices = i2c.scan()

    #TODO: Print addresses
    #print(str(devices))

    #save address of IMU (0x28 by default for BNO055)
    imuAddr = 0x28

    #Create IMU class
    imu = IMU(i2c, imuAddr)

    print("IMU Initialized")

    #Check Calibration statuses
    imu.checkCalib()

    while(running):
    
        #Get Euler tuple
        euler = imu.getOrientation()

        #Save individual values
        heading = euler[0] 
        roll = euler[1] 
        pitch = euler[2]

        #print Euler angles
        print("Euler Angles\n-------------------")
        print("heading: " + str(heading))
        print("roll: " + str(roll))
        print("pitch: " + str(pitch))

        timer += 1

        #The program has run for 15s
        if(timer >= 15000):
            running = False

    print("Ending IMU Test")


if __name__ == '__main__':
    main()

