''' @file imu.py
    This file contains the Interial Measurement Unit Class that provides functions
    to easily connect and interace with the BNO055 IMU over I2C'''

import pyb
from pyb import I2C

class IMU:
    ''' This class implements an IMU driver for the 
    STM32L476 Nucleo-64 development board.'''
    i2c = None
    addr = None

    #Number of bytes to read
    ONE_BYTE = 1
    TWO_BYTES = 2

    #Power Mode Register and values
    PWR_MODE_REG = 0x3E 
    NORMAL_MODE = 0x00
    SUSPEND_MODE = 0x02

    #Operation Mode Register and values
    OPR_MODE_REG = 0x3D
    FUSION_MODE = 0x08

    #Calibration Status Register
    CALIB_STAT = 0x35

    #Euler Vector Registers
    HEADING = 0x1A
    ROLL = 0x1C
    PITCH = 0x1E

    #Angular Velocity (Gyroscope) Registers
    GYR_X = 0x14
    GYR_Y = 0x16
    GYR_Z = 0x18 

    # Calibration Bit Masks, Shift Values, and Calibrated values
    BIT_MASK_2 = 2
    SYS_SHIFT = 6  
    GYR_SHIFT = 4
    ACC_SHIFT = 2
    MAG_SHIFT = 0
    CALIBRATED = 2
    NOT_CALIBRATED = 0

    def __init__(self, i2c, imuAddr):
        ''' Initialized an IMU object and stores the I2C communication object
            as well as the IMU's address. Once initialized, the IMU is enable.
        
        @param i2c An I2C object to be used for communication
        @param imuAddr The I2C address of the the imu to be controlled'''

        self.i2c = i2c
        self.addr = imuAddr
        self.enable()
        
        print('finish me')
    
    def enable(self):
        ''' This function enables the IMU by initializing hane I2C connection, setting the IMU into normal mode,
        and selection IMU for the mode of operation.'''
        self.i2c.init(I2C.MASTER)
        
        #Sets PWR_MODE reg to normal mode
        self.i2c.mem_write(self.NORMAL_MODE, self.addr, self.PWR_MODE_REG, timeout=5000, addr_size=8)

        #Set OPR_Mode Reg to fusion mode
        self.i2c.mem_write(self.FUSION_MODE, self.addr, self.OPR_MODE_REG, timeout=5000, addr_size=8)

        # Note: might need to wait 7ms while switching to op mode from config
        # Note: might need to wait 19 ms while switching to config from op

        # mem_read(data, addr, memaddr, *, timeout=5000, addr_size=8)
        # mem_write(data, addr, memaddr, *, timeout=5000, addr_size=8)
        # recv(recv, addr=0x00, *, timeout=5000)
        # send(send, addr=0x00, *, timeout=5000)

    def disable(self):
        ''' This function disable the IMU by putting it into suspend mode and closing the I2C connection'''
        #Sets PWR_MODE reg to suspend mode 
        self.i2c.mem_write(self.SUSPEND_MODE, self.addr, self.PWR_MODE_REG, timeout=5000, addr_size=8)

        #Close I2C connection
        self.i2c.deinit()

    def setMode(self, modeCode):
        ''' Changes the IMU into the mode specified by the modeCode

        @param modeCode The mode value that the IMU should be sent to mode register of the IMU'''
        
        #Set OPR_Mode Reg to given modeCode
        self.i2c.mem_write(modeCode, self.addr, self.OPR_MODE_REG, timeout=5000, addr_size=8)


    def checkCalib(self):
        ''' This function checks the calibration status of the IMU'''    

        # Read all calibration values from calib stat
        allCalibs = self.i2c.mem_read(self.ONE_BYTE, self.addr, self.CALIB_STAT, timeout=5000, addr_size=8)

        print(allCalibs)
        
        allCalibs = bytes(allCalibs)
        
        print(allCalibs)
    
        # The Calibration data is 8 bits: Four 2bit groups. This needs to be split up.
        # Note: A 3 means fully calibrated. A 0 means uncalibrated
        
        # Determine calibration status for system
        system = (allCalibs >> self.SYS_SHIFT) & self.BIT_MASK_2
        sysStatus = self.calibHelper(system)

        # Determine calibration status for gyroscope
        gyro = (allCalibs >> self.GYR_SHIFT) & self.BIT_MASK_2
        gyroStatus = self.calibHelper(gyro)

        # Determine calibration status for accelerometer
        accel = (allCalibs >> self.ACC_SHIFT) & self.BIT_MASK_2
        accelStatus = self.calibHelper(accel)       

        # Determine calibration status for magnetometer
        magnet = (allCalibs >> self.MAG_SHIFT) & self.BIT_MASK_2
        magnetStatus = self.calibHelper(magnet)
        
        # Print all statuses
        print("System is " + sysStatus)
        print("Gyroscope is " + gyroStatus)
        print("Accelerometer is " + accelStatus)
        print("magnetometer is " + magnetStatus)

    def calibHelper(self, calibObject):
        ''' This function is used to help checkCalib(). It checks if the valued for calibObject is
            2 (meaning calibrated) or 0 (meaning not calibrated)
        
        @param calibObject The sensor we want to determine the calibration status for.''' 
        status = None

        if (calibObject == self.CALIBRATED):
            status = 'calibrated'
        else:
            status = 'not calibrated'

        return status

    def getOrientation(self):
        ''' This function requests three Euler angles from the IMU and
        returns them as a tuple.'''        

        # Read heading
        heading = self.i2c.mem_read(self.TWO_BYTES, self.addr, self.HEADING, timeout=5000, addr_size=8)
        heading = int(heading)

        # Read Roll
        roll = self.i2c.mem_read(self.TWO_BYTES, self.addr, self.ROLL, timeout=5000, addr_size=8)
    
        # Read Pitch
        pitch = self.i2c.mem_read(self.TWO_BYTES, self.addr, self.PITCH, timeout=5000, addr_size=8)

        return (heading, roll, pitch)     

    def getAngularVelocity(self):
        ''' This function requests three angular velocity values from 
        the gyroscope on the IMU and returns them as a tuple.'''        

        #Read Angular velocity in X direction
        velX = self.i2c.mem_read(self.TWO_BYTES, self.addr, self.GYR_X, timeout=5000, addr_size=8)

        #Read Angular velocity in Y direction
        velY = self.i2c.mem_read(self.TWO_BYTES, self.addr, self.GYR_Y, timeout=5000, addr_size=8)

        #Read Angular velocity in Z direction
        velZ = self.i2c.mem_read(self.TWO_BYTES, self.addr, self.GYR_Z, timeout=5000, addr_size=8)
        return (velX, velY, velZ)

