''' @file mainpage.py
    @author Gonzalo Arana
    @mainpage
    @section sec_intro Introduction
    Lab 4 - Intertial Measurement Unit Controller
    This lab focuses on creating an IMU Controller to determine the orientation and
    angular velocity of an object with the BNO055 chip attached to it.

    A Demo of this can be found at: https://youtu.be/dTfyfs5ldCg

    @section Purpose
    This controller is intended to track an objects orientation and angular velocity.
    
    @section Usage
    The IMU Controller object can be used by importing IMUfrom imu.py.
    An IMU object takes and stores an I2C object and the address of the BNO055 device
    connected to the I2C bus.
    
    @section Testing
    This code was tested by connecting a BNO055 board and rotating it while observing the 
    orientation printed by the included main.py. The calibration statuses were also printed
    for each unit in the board.
    
    @section Bugs and Limitations
    The setMode() function does not currentl check to ensure that the user does not
    input an invalid mode.

    @section Location
    All code can be found at the link below.
    Link to code: https://bitbucket.org/gonzaloarana/me405-03/src/master/lab4/
'''
